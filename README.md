### RC Notes

1. All dependencies are distributed into individual classes, configurable at time of call in main.php.
1. Interfaces are applied where possible extension may be done.
1. Added function forceEUR to ensure key with value 1; exchangeratesapi did not return EUR key at time of development.
1. Moved EU countries list to data file; added class to handle it.
1. added postProcess in CommissionList to apply ceiling at 2 decimal places, after commission is returned.
1. This package is ready for docker setup:
   1. from application root path, eg: `~/dev/pustudy>`
   ```
   # docker-compose up --build -d
   # docker attach pustudy_php_1   
   ```
   1. To run the application in container:
   ```
   /var/www # php www/main.php
   1
   0.43
   1.61
   2.21
   44.28
   ```
1. Unit tests can be run from app root path in container:
   ```
   /var/www # ./vendor/bin/phpunit --verbose --testdox tests
   ```


<?
require __DIR__ . '/../vendor/autoload.php';

use PUStudy\DAO\logSingle;
use PUStudy\DAO\TransactionTextFile;
use PUStudy\DAO\BinList;
use PUStudy\DAO\EUListJSONFile;
use PUStudy\Model\CommissionList;
use PUStudy\DAO\ExchangeratesAPIIO;
use PUStudy\Model\StandardCommission;

set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$comList = new CommissionList(
    new TransactionTextFile(__DIR__ . '/../data/transactions_20200804.txt'),
    new BinList(true),
    new EUListJSONFile(__DIR__ . '/../data/eu_countries.json'),
    new ExchangeratesAPIIO(true),
    new StandardCommission()
);

$comList->processTRXBatch(true);
$comList->displayCommissionsList();

echo logSingle::displayLogs();

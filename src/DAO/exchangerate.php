<?php

namespace PUStudy\DAO;

interface exchangerate {
    function getRate($currency): float;
}

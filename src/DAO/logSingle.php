<?php
namespace PUStudy\DAO;

final class logSingle
{
    private static ?logSingle $instance = null;
    public static array $logs = [];

    /**
     * gets the instance via lazy initialization (created on first usage)
     * @param $logentry
     * @return logSingle
     */
    public static function getInstance(?string $logentry = null): logSingle
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        if ($logentry <> null)
            static::$logs[] = $logentry;
        return static::$instance;
    }

    public static function displayLogs() {
        $retVal = "\nApplication messages:\n";
        if (count(static::$logs) > 0) {
            $i=0;
            foreach(static::$logs as $msg) {
                $retVal .=  ++$i.". $msg\n";
            }
            return $retVal;
        } else {
            $retVal .= "No messages found.\n";
            return $retVal;
        }
    }

    public static function getLogs() {
        return static::$logs;
    }

    public static function clearLogs()
    {
        static::$logs=[];
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances
     */
    private function __construct()
    {
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone()
    {
    }

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup()
    {
    }
}

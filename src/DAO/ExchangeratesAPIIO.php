<?php

namespace PUStudy\DAO;

use ErrorException;

class ExchangeratesAPIIO implements exchangerate
{
    private $exchRateURI = 'https://api.exchangeratesapi.io/latest';
    private $exchRates = [];
    private $localExchFile = __DIR__ . '/../../data/exchangerates.json';

    public function __construct(bool $getLocal)
    {
        if ($getLocal)
            $this->loadLocal();
        else
            $this->loadRemote();

        $this->forceEUR();
    }

    private function loadLocal() {
        try {
            $localResults = file_get_contents($this->localExchFile);
            $this->exchRates = json_decode($localResults,true)["rates"];
        } catch (\Exception $e) {
            $applog = logSingle::getInstance( $e->getMessage());
        }
    }

    private function forceEUR() {
//        added since it's missing from exchangeratesapi list
        $this->exchRates['EUR'] = 1;
    }

    private function loadRemote() {
        try {
            $exchResults = file_get_contents($this->exchRateURI);
            $exchRateDecoded = @json_decode($exchResults);
            if ( property_exists ( $exchRateDecoded, "rates") ){
                $this->exchRates = $exchRateDecoded["rates"];
            }
            else {
                $appmsg = 'Invalid array decoded from '.$this->exchRateURI;
                $applog = logSingle::getInstance( $appmsg );
            }
        } catch (\Exception $e) {
            $applog = logSingle::getInstance( $e->getMessage() );
        }
    }

    public function getRate($currency): float
    {
        $retVal = false;

        try {
            $retVal = $this->exchRates[$currency];
        } catch (ErrorException $e) {
            $applog = logSingle::getInstance( "No matching currency $currency; " . $e->getMessage() );
        }
        return $retVal;
    }
}
<?php
namespace PUStudy\DAO;

class BinList implements binlookup {

    public $baseURL = 'https://lookup.binlist.net/';
    public $binList = [];
    private $localBINFile = __DIR__ . '/../../data/binalpha2.json';


    public function __construct(bool $loadLocal=false, ?string $otherURL = null)
    {
        if ($otherURL <> null)
            $this->baseURL = $otherURL;
        if ($loadLocal) $this->loadLocalFile();
    }

    private function loadLocalFile() {
        $binResults = file_get_contents($this->localBINFile);
        $this->binList = json_decode($binResults,true);
    }

    private function checkLocal(string $BINum) {
        if (key_exists($BINum,$this->binList))
            return $this->binList[$BINum];
        else
            return false;
    }

    private function checkRemote($BINum) {
        $returnVal = false;
        try {

            $binResults = file_get_contents($this->baseURL.$BINum);
            $binObj = @json_decode($binResults);
            if (
                property_exists ( $binObj,          "country") &&
                property_exists ( $binObj->country, "alpha2")
            ){
                $returnVal = $binObj->country->alpha2;
            }
            else {
                $appmsg = "Can't read results from ".$this->baseURL.$BINum;
                $applog = logSingle::getInstance($appmsg);
            }
        } catch (\ErrorException $e) {
            $applog = logSingle::getInstance(
                "Error with " . $this->baseURL.$BINum . "; " . $e->getMessage()
            );
        }
        return $returnVal;
    }

    public function getBinAlpha2(string $BINum): string
    {
        $local = $this->checkLocal($BINum);
        if ($local === false) {
            $binAlpha2 = $this->checkRemote($BINum);
            if ($binAlpha2 != false)
            $this->binList[$BINum] = $binAlpha2;
            return $binAlpha2;
        }
        else
            return $local;
    }
}
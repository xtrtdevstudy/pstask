<?php
namespace PUStudy\DAO;

class EUListJSONFile implements eulist {
    private string $sourceFilename;
    private $sourceFileExists = false;
    private $EUList = [];

    public function __construct(string $sourceFile)
    {
        if (!file_exists($sourceFile)){
            $appmsg = 'File does not exist. Path: '.$sourceFile;
            $applog = logSingle::getInstance( $appmsg );
        }
        else {
            $this->sourceFilename = $sourceFile;
            $this->sourceFileExists = true;
            $this->setA2List();
        }
    }
    public function setA2List()
    {
        if ($this->sourceFileExists) {
            try {
                $this->EUList = json_decode(
                    file_get_contents($this->sourceFilename),
                    true
                );
            } catch (\Exception $e) {
                $appmsg = $e->getMessage(). ': cannot decode json. Path: '.$this->sourceFilename;
                $applog = logSingle::getInstance( $appmsg );
            }
        }
    }
    public function isEU($alpha2): bool
    {
        return $isEu = in_array($alpha2,$this->EUList);
    }
}
<?php
namespace PUStudy\DAO;

use PUStudy\DAO\transactions;

class TransactionTextFile implements transactions
{
    private $sourceFilename;
    private $sourceFileExists = false;

    public function __construct($sourceFile)
    {
        if (!file_exists($sourceFile)){
            $appmsg = 'File does not exist. Path: '.$sourceFile;
            $applog = logSingle::getInstance( $appmsg );
        }
        else {
            $this->sourceFilename = $sourceFile;
            $this->sourceFileExists = true;
        }
    }

    public function getData(): array
    {
        $returnArray = [];
        if ($this->sourceFileExists) {
            $handle = fopen($this->sourceFilename, "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (!empty($line)) {
                        try {
                            $lineArray = json_decode($line, true, 512, JSON_THROW_ON_ERROR);
                            $returnArray[] = $lineArray;
                        } catch (\JsonException $e) {
                            $appmsg = $e->getMessage() . ': This line is not valid JSON: ' . $line;
                            $applog = logSingle::getInstance($appmsg);
                        }
                    }
                }
                fclose($handle);
            }
            if (count($returnArray) == 0)
                $applog = logSingle::getInstance("No transactions found.");
        }
        return $returnArray;
    }
}

<?php
namespace PUStudy\Model;

use PUStudy\DAO\binlookup;
use PUStudy\DAO\exchangerate;
use PUStudy\DAO\logSingle;
use PUStudy\DAO\transactions;
use PUStudy\DAO\eulist;
use PUStudy\Model\PostProcess;


class CommissionList {

    private array $trxList;
    private array $commissionList = [];
    private binlookup $binlookup;
    private eulist $eulist;
    private exchangerate $exchRates;
    private computecommission $computeCommission;
    private bool $setCeiling;

    public function __construct(
        transactions $trxListFile,
        binlookup $binlookup,
        eulist $eulist,
        exchangerate $exchRate,
        computecommission $compCom
    )
    {
        $this->trxList = $trxListFile->getData();
        $this->binlookup = $binlookup;
        $this->eulist = $eulist;
        $this->exchRates = $exchRate;
        $this->computeCommission = $compCom;
    }

    public function processTRXBatch( bool $setCeiling = false) {
        $this->setCeiling = $setCeiling;
        foreach($this->trxList as $row) {
            [$trx_bin, $trx_amt, $trx_currency] = array_values($row);
            $alpha2 = $this->binlookup->getBinAlpha2($trx_bin);
            $exRate = $this->exchRates->getRate($trx_currency);
            if ($alpha2 == false || $exRate == false ) continue;
            $computeResult = $this->computeCommission->compute(
                $trx_amt,
                $trx_currency,
                $exRate,
                $this->eulist->isEU($alpha2)
            );
            $this->commissionList[] = $setCeiling ?
                PostProcess::ceiling2decimal($computeResult) :
                $computeResult;
        }
    }

    public function displayCommissionsList() {
        foreach ($this->commissionList as $row) {
            echo $row."\n";
        }
    }

    public function getCommissionsList() {
        return $this->commissionList;
    }
}
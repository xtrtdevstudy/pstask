<?php

namespace PUStudy\Model;

final class PostProcess
{
    public static function ceiling2decimal(float $amt): float {
        return ceil($amt * 100)/100;
    }

}
<?php

namespace PUStudy\Model;

interface computecommission {
    function compute(float $amt, string $cur, float $rate, bool $isEU);
}

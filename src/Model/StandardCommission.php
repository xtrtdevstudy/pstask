<?php

namespace PUStudy\Model;

class StandardCommission implements computecommission {

    public function compute(float $amt, string $cur, float $rate, bool $isEU) {
        switch($cur) {
            case 'EUR':
                $amtFixed = $amt;
            default:
                if ($rate <= 0)
                    $amtFixed = $amt;
                else
                    $amtFixed = $amt / $rate;
        }
        return $amtFixed * ($isEU ? 0.01 : 0.02);
    }
}

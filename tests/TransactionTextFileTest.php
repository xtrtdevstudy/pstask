<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\DAO\TransactionTextFile;
use PUStudy\DAO\logSingle;

final class TransactionTextFileTest extends TestCase
{
    public function testValidFile(): void
    {
        logSingle::clearLogs();
        $trxData = (new TransactionTextFile(__DIR__ . '/transactions_20200804.txt'))->getData();
        $this->assertIsArray($trxData);
        $this->assertCount(5, $trxData);
        $this->assertCount(0,logSingle::getLogs());
        logSingle::clearLogs();
    }

    public function testValidFileWithInvalidLine(): void
    {
        logSingle::clearLogs();
        $trxData = (new TransactionTextFile(__DIR__ . '/transactions_20200804_werror.txt'))->getData();
        $this->assertIsArray($trxData);
        $this->assertCount(4, $trxData);
        $this->assertEquals("
Application messages:
1. Syntax error: This line is not valid JSON: {\"bin\":\"41417360\",\"amount:\"130.00\",\"currency\":\"USD\"}

",logSingle::displayLogs());
        logSingle::clearLogs();
    }

    public function testNoFileExists(): void
    {
        logSingle::clearLogs();
        $trxData = (new TransactionTextFile(__DIR__ . '/nofileexists.txt'))->getData();
        $this->assertIsArray($trxData);
        $this->assertCount(0, $trxData);
        $this->assertEquals("
Application messages:
1. File does not exist. Path: /var/www/tests/nofileexists.txt
",logSingle::displayLogs());
        logSingle::clearLogs();
    }
}


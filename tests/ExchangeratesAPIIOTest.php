<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\DAO\ExchangeratesAPIIO;
use PUStudy\DAO\logSingle;
require_once "CustomErrorHandler.php";

final class ExchangeratesAPIIOTest extends TestCase
{

    public function testValidLocalFile(): void
    {
        logSingle::clearLogs();
        $exr = new ExchangeratesAPIIO(true);
        $this->assertCount(0,logSingle::getLogs());
        logSingle::clearLogs();
    }

    public function testValidCurrencyQuery(): void
    {
        logSingle::clearLogs();
        $exr = new ExchangeratesAPIIO(true);
        $this->assertEquals(1.5773,$exr->getRate('CAD'));
        logSingle::clearLogs();
    }

    public function testInvalidCurrencyQuery(): void
    {
        $logfile = new CustomErrorHandler();
        logSingle::clearLogs();
        $exr = new ExchangeratesAPIIO(true);
        $this->assertEquals(0,$exr->getRate('QXR'));
        logSingle::clearLogs();
    }
}


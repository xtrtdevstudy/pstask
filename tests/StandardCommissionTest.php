<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\Model\StandardCommission;
use PUStudy\Model\PostProcess;
use PUStudy\DAO\logSingle;
require_once "CustomErrorHandler.php";

final class StandardCommissionTest extends TestCase
{
    public function testEuroCurrency(): void
    {
        logSingle::clearLogs();
        $commission = (new StandardCommission())->compute(1000,'EUR',1,true);
        $this->assertEquals(10,$commission);
        logSingle::clearLogs();
    }

    public function testUSD_At_1_1765_Rate(): void {
        $logger = new CustomErrorHandler();
        $commission = PostProcess::ceiling2decimal(
            (new StandardCommission())->compute(1000,'USD',1.1765,false)
        );
        $this->assertEquals(17,$commission);
        logSingle::clearLogs();
    }
}


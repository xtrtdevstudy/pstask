<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\DAO\BinList;
use PUStudy\DAO\logSingle;
require_once "CustomErrorHandler.php";

final class BinListTest extends TestCase
{
    public function testValidMockFileWith5Entries(): void
    {
        logSingle::clearLogs();
        $alpha2Data = (new BinList(true))->getBinAlpha2('45717360');
        $this->assertEquals('DK',$alpha2Data);
        $alpha2Data = (new BinList(true))->getBinAlpha2('516793');
        $this->assertEquals('LT',$alpha2Data);
        $alpha2Data = (new BinList(true))->getBinAlpha2('45417360');
        $this->assertEquals('JP',$alpha2Data);
        $alpha2Data = (new BinList(true))->getBinAlpha2('41417360');
        $this->assertEquals('US',$alpha2Data);
        $alpha2Data = (new BinList(true))->getBinAlpha2('4745030');
        $this->assertEquals('GB',$alpha2Data);
        $this->assertCount(0,logSingle::getLogs());
        logSingle::clearLogs();
    }

    public function testUseMockUrl(): void {
        $logger = new CustomErrorHandler();
        logSingle::clearLogs();
        $alpha2Data = (new BinList(true,'https://lookup.mockbinlist.mock/'))->getBinAlpha2('1111');
        $this->assertEquals(false,$alpha2Data);
        $this->assertEquals("
Application messages:
1. Error with https://lookup.mockbinlist.mock/1111; file_get_contents(): php_network_getaddresses: getaddrinfo failed: Name does not resolve
",logSingle::displayLogs());
        logSingle::clearLogs();
    }
}


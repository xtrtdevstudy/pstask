<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\DAO\logSingle;
use PUStudy\DAO\TransactionTextFile;
use PUStudy\DAO\BinList;
use PUStudy\DAO\EUListJSONFile;
use PUStudy\Model\CommissionList;
use PUStudy\DAO\ExchangeratesAPIIO;
use PUStudy\Model\StandardCommission;
require_once "CustomErrorHandler.php";

final class CommissionListTest extends TestCase
{
    public function testValidMockFileWith5Entries(): void
    {
        $logger = new CustomErrorHandler();
        $comList = new CommissionList(
            new TransactionTextFile(__DIR__ . '/../data/transactions_20200804.txt'),
            new BinList(true),
            new EUListJSONFile(__DIR__ . '/../data/eu_countries.json'),
            new ExchangeratesAPIIO(true),
            new StandardCommission()
        );
        $comList->processTRXBatch(true);
        $this->assertEquals(5,count($comList->getCommissionsList()));
        $this->assertCount(0, logSingle::getLogs());
        logSingle::clearLogs();
    }
}

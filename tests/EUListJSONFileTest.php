<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
use PUStudy\DAO\EUListJSONFile;
use PUStudy\DAO\logSingle;

final class EUListJSONFileTest extends TestCase
{
    public function testValidFileEuCountry(): void
    {
        logSingle::clearLogs();
        $eulist = new EUListJSONFile(__DIR__ . '/../data/eu_countries.json');
        $this->assertCount(0,logSingle::getLogs());
        $this->assertEquals(true,$eulist->isEU("FR"));
        logSingle::clearLogs();
    }

    public function testValidFileNonEuCountry(): void
    {
        logSingle::clearLogs();
        $eulist = new EUListJSONFile(__DIR__ . '/../data/eu_countries.json');
        $this->assertEquals(false,$eulist->isEU("US"));
        logSingle::clearLogs();
    }

    public function testNoFileExists(): void
    {
        logSingle::clearLogs();
        $eulist = new EUListJSONFile(__DIR__ . '/../data/eu_countries_NOFILE.json');
        $this->assertEquals("
Application messages:
1. File does not exist. Path: ".__DIR__ . "/../data/eu_countries_NOFILE.json
",logSingle::displayLogs());
        logSingle::clearLogs();
    }
}


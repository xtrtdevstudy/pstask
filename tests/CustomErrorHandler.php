<?php

class CustomErrorHandler {
    public function __construct() {
        // Try commenting out this line:
        set_error_handler(array($this, 'handleError'));
    }

    public function __desctruct() {
        restore_error_handler();
    }

    public function handleError($errno, $errstr, $errfile = null, $errline = null, $errcontext = null) {
        // error was suppressed with the @-operator
        if (0 === error_reporting()) {
            return false;
        }
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
}
